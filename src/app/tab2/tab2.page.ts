import { Component } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { localStorageController } from '../localstorage';
import { Card } from '../card';
import { HttpClient } from '@angular/common/http';

@Component({
    selector: 'app-tab2',
    templateUrl: 'tab2.page.html',
    styleUrls: ['tab2.page.scss', 'dark.css']
})
export class Tab2Page {

    constructor(public toastController: ToastController, private http: HttpClient) {}

    storageController = new localStorageController();
    selectedItem: Card;
    randomButtons;
    // setting an URL to use if no picture can be found for the first Card (if no image can be found for any other card, it will just keep displaying the previous one)
    imageUrl: string = "https://cdn.pixabay.com/photo/2020/06/05/01/28/compass-5261062_960_720.jpg";

    ngOnInit() {
        // get random buttons (answers) at startup (so that you have someting to press on)
        this.randomize();
    }

    public isDarkColor() {
      if (localStorage.getItem('dark') == 'true')
        return "";
      return "light";
    }

    public randomize() {
        // select a random item
        this.selectedItem = this.storageController.getOneCardFromLocalStorage();
        // get random buttons (all the answers)
        this.getRandomButtons();

        var settingSaveSearch = "";
        settingSaveSearch = "&safesearch=true"; // sets the Api to only show SFW images (comment out this line, if you want to show them)

        // get the response JSON from the API (per_page is 3, because the API doesn't allow for anything lower than that)
        this.http.get("https://pixabay.com/api/?key=10937717-4c60c4f4ce95a9e4164402d21" + settingSaveSearch +"&q=" + this.selectedItem.wordForeign + "&per_page=3&orientation=horizontal").subscribe( (data: any) => {
            // set the imageUrl to the webformatURL of the first iteam received from the API, in order to display an image of the item to be learned
            this.imageUrl = data.hits[0].webformatURL;
        });

        // reset the color of all buttons (so none of them are marked as already pressed again)
    document.querySelectorAll("ion-button").forEach(element => { element.color = (localStorage.getItem('dark') == 'true') ? "" : "light" });
    }

    async choose(item, event) {
        // display message if the user clicked on the correct button
        const toast = await this.toastController.create({
          message: "Your answer was " + ((item == this.selectedItem.wordForeign) ? "correct" : "incorrect"),
          duration: 350
        });
        toast.present();
        

        // functions that will be executed, depending on if the user has pressed the right button or the wrong one
        if (item == this.selectedItem.wordForeign){
            this.randomize();
            
            // execute Samuels statistics code
            this.storageController.addAnswerData(true);
            this.storageController.somethingWasLearned();
        }
        else {
            // this brightens the button, in oder to give the user visual feedback about which buttons where already pressed
            event.target.color = "none";
            this.storageController.addAnswerData(false);
        }
    }

    // this function returns a list of max 9 random entries of foreign words which includes the correct one
    // (a return is not necesairy, beause this function updates a class-variable, which angular detects and updates the view automatically)
    getRandomButtons() {
        var allCards = [];

        // add every wordForeign to all cards (all other attributes of Card are not important for this function and will be ignored)
        this.storageController.getAllCardsFromLocalStorage().forEach(element => {
            allCards.push(element.wordForeign);
        });

        var outputCards = [];

        // make sure, that the correct answer will always be added
        outputCards.push(this.selectedItem.wordForeign);

        // add up to 8 additional cards (bringing the total up to 9) while making sure that no dublicates will be added
        // (this also conveniently means that the correct answer won't be added twice)
        for (let i = 0; i < allCards.length && outputCards.length != 8; i++)
            if (!outputCards.includes(allCards[i]))
                outputCards.push(allCards[i]);

        // shuffle the output and save it into the array
        this.randomButtons = this.shuffle(outputCards);
    }

    /* Randomize array in-place using Durstenfeld shuffle algorithm
       copied from https://stackoverflow.com/a/2450976           */
    private shuffle(array) {
        var currentIndex = array.length, temporaryValue, randomIndex;
        // While there remain elements to shuffle...
        while (0 !== currentIndex) {
            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;
            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }
        return array;
    }
}
