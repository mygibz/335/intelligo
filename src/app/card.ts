export class Card {
    public id: string;
    public imageUrl: string;
    public wordLocal: string;
    public wordForeign: string;
    public description: string;

    constructor(wordLocal, wordForeign, description, imageUrl, id=null) {
        if (id == null)
            this.id = Math.random().toString(36);
        else
            this.id = id;
            
        this.wordLocal = wordLocal;
        this.wordForeign = wordForeign;
        this.description = description;
        this.imageUrl = imageUrl;
    }
}