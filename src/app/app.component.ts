import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss','dark.css']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
  }

  // TODO: move to a central location
  public isDark() {
    if (localStorage.getItem('dark') == 'true')
      return "dark";
    return "";
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.splashScreen.hide();
      this.statusBar.overlaysWebView(false);
      this.setDarkModeTitlebar();
      });
  }
  
  public setDarkModeTitlebar() {
    if(this.isDark() == 'dark') {
      this.statusBar.backgroundColorByHexString('#2E3440');
      this.statusBar.styleBlackOpaque();
    } else {
      this.statusBar.backgroundColorByHexString('#ffffff');
      this.statusBar.styleDefault();
    }
  }
}
