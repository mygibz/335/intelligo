import { StatusBar } from '@ionic-native/status-bar/ngx';

export class DarkMode {

  constructor (private statusBar: StatusBar = new StatusBar()) {
    if (localStorage.getItem('dark') == 'true') {
        this.statusBar.backgroundColorByHexString('#2E3440');
        this.statusBar.styleBlackOpaque();
    } else {
        this.statusBar.backgroundColorByHexString('#ffffff');
        this.statusBar.styleDefault();
    }
  }
}
