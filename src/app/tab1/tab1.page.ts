import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { Chart } from 'chart.js';
import { localStorageController } from '../localstorage';


@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss','dark.css']
})
export class Tab1Page implements OnInit{

    localStorageController = new localStorageController;

    constructor() {}

    ngOnInit() {
        this.writeCharts();
    }

    public writeCharts(){
        // create line-chart
        var line = (document.getElementById('lineChart') as HTMLCanvasElement).getContext('2d');
        new Chart(line, {
        type: 'line',
        data: {
            labels: this.localStorageController.getGraphXData(),
            datasets: [{
                label: 'Cards learned',
                data: this.localStorageController.getGraphYData(),
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(0,255,255,0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)',
                    'rgba(0,255,255,1)'

                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        stepSize: 1
                    }
                }]
            }
        }
    });
    // create pie-chart
    var pie = (document.getElementById('pieChart') as HTMLCanvasElement).getContext('2d');
        new Chart(pie, {
        type: 'pie',
        data: {
            labels: ["Correct ("+this.localStorageController.getAnswerAccuracy()[0]+"%)", "Wrong ("+this.localStorageController.getAnswerAccuracy()[1]+"%)"],
            datasets: [{
                label: 'Correct and wrong answers',
                data: this.localStorageController.getAnswerAccuracy(),
                backgroundColor: [,
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)'
                ],
                borderColor: [
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 99, 132, 1)'
                ],
                borderWidth: 1
            }]
        }
    });
  }

}
