import { Component } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';
import { localStorageController } from '../localstorage';
import { AddCardPage } from '../add-card/add-card.page';
import { EditCardPage } from '../edit-card/edit-card.page';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss','dark.css']
})
export class Tab3Page {

  constructor(public modalController: ModalController, private toastController: ToastController) {}

  storageController = new localStorageController();

  // this function gets all cards from the local storage and groups them by there first letter
  getAllCardsFromLocalStorage() {
    // get all cards in the localstorage and put them into an array called list
    var list = this.storageController.getAllCardsFromLocalStorage();
    
    var output = [];

    for(var i = 0; i < list.length; i++) {
      // get first letter of substring and get it's ASCII number, to be used as the index
      var firstLetter = list[i].wordForeign.substring(0,1).toUpperCase().charCodeAt(0);
      // create array if not existant
      if (!output[firstLetter]) output[firstLetter] = [];
      // append to output
      output[firstLetter].push(list[i]);
    }

    // remove empty arrays
    output = output.filter(Boolean);

    return output;
  }

  async addModal() {
    // create a modal
    const modal = await this.modalController.create({component: AddCardPage});
    modal.present();
  }
  
  async editModal(id: string) {
    // create a modal and pass in the id of the selected card
    const modal = await this.modalController.create({
      component: EditCardPage,
      componentProps: { original: this.storageController.getSpecificCardFromLocalStorage(id) }
    });
    modal.present();
  }

  async removeCard(id: string) {
    // check, that at least one card will be remaining
    if (this.storageController.getAllCardsFromLocalStorage().length <= 1) {
      const toast = await this.toastController.create({
        message: "You can't delete the last card, at least one card is required",
        duration: 500
      });
      toast.present();
    }
    else
      // remove a card from local storage by it's id
      this.storageController.removeCard(id);
  }
}
