# Intelligo

### Description

Intelligo ist eine einfache App um Lernstoff mithilfe von Karteikarten zu lernen.
Die App ermöglicht es dem Nutzer, eigene Karteikarten zu erfassen, zu lernen und 
die dazugehörigen Statistiken einzusehen.